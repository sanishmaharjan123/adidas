<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Aauth');
        $this->load->library('email');
        $this->load->helper('email');

    }

    public function auth_get()
    {

    }

    public function create_user_post()
    {
        $fullname = $this->input->post('fullname');
        $password = $this->input->post('password');

        $customer['email'] = $this->input->post('email');
        $customer['mobile'] = $this->input->post('phone');
        $customer['security_question'] = $this->input->post('security_question');
        $customer['answer'] = $this->input->post('answer');

        $verification_type = $this->input->post('verification_type');

        $user_id = $this->aauth->create_user($customer['email'],$password,NULL,$fullname,NULL);
        
        if($user_id)
        {
            $customer['user_id'] = $user_id;
            $customer['created_by'] = $user_id;
            $customer['created_at'] = date('Y-m-d H:i:s');
            $this->db->insert('tbl_customer_details',$customer);

            
            $banned['banned'] = 1;

            $this->db->where('id', $user_id);
            $this->db->update('aauth_users', $banned);

                // sends verifition ( !! e-mail settings must be set)
            if($verification_type == 'email')
            {
                $this->send_verification($user_id);
                echo json_encode(array('user_id'=>$user_id,'verification_code'=>NULL));
            }
            else
            {
                $verification_code = strtoupper(random_string('alnum', 6));
                $this->db->where('id', $user_id);
                $this->db->update('aauth_users', array('verification_code' => $verification_code));

                echo json_encode(array('user_id'=>$user_id,'verification_code'=>$verification_code));
            }
            $this->update_user($user_id); 
        }

        $refer_code = $this->input->post('refer_code');
        $this->db->where('referral_code',$refer_code);
        $refer_user_id = $this->db->get('aauth_users')->row();

        print_r($refer_user_id);
        exit;
    }

    public function login_post()
    {
        $facebook_id = $this->input->post('fb_id');
        $this->db->where('facebook_id',$facebook_id);

        $check_user = $this->db->get('aauth_users')->row();
        if(empty($check_user))
        {
            $email = $this->input->post('email');
            $fullname = $this->input->post('fullname');
            $password = 'password123';
            $fb_image = $this->input->post('fb_image');
            $user_id = $this->aauth->create_user($email,$password,NULL,$fullname,$facebook_id);
            if($user_id)
            {
                $customer['created_by'] = $user_id;
                $customer['created_at'] = date('Y-m-d H:i:s');
                $customer['email'] = $email;
                $customer['fb_image_url'] = $fb_image;
                $this->db->insert('tbl_customer_details',$customer);

                $this->update_user($user_id);
            }
        }
        else
        {
            $result = $this->fast_login($check_user->id);
        }
        
        echo json_encode($result);
    }

    public function auth_delete()
    {

    }

    private function _generate_token()
    {
        return md5(uniqid(rand(10000000, 99999999), true));
    }

    private function fast_login($user_id)
    {
        $token = $this->_generate_token();
        $data = array('access_token'=>$token);
        $this->db->where('id',$user_id);
        $this->db->update('aauth_users',$data);

        $this->db->where('user_id',$user_id);
        $data  = $this->db->get('view_customer_details')->row();
        return $data;
    }

    private function _referral_code()
    {
        $referral_code = substr(md5(uniqid(rand(1000000, 9999999), true)),1,7);
        $this->db->where('referral_code',$referral_code);
        $check_duplicate = $this->db->get('aauth_users')->row();
        if($check_duplicate)
        {
            $this->_referral_code();
        }
        else
        {
            return $referral_code;
        }
    }

    private function update_user($user_id)
    {
        $user['user_id'] = $user_id;
        $user['group_id'] = 102;
        $this->db->insert('aauth_user_groups',$user);


        // update referral code
        $update_user['referral_code'] = $this->_referral_code();

        $this->db->where('id',$user_id);
        $this->db->update('aauth_users',$update_user);

        $result = $this->fast_login($user_id);
    }

    public function send_verification($user_id){
        $query = $this->db->where( 'id', $user_id );
        $query = $this->db->get('aauth_users');
        
        if ($query->num_rows() > 0){
            $row = $query->row();

            $verification_code = strtoupper(random_string('alnum', 6));

            $data['site_name'] = 'Adidas';
            $data['fullname'] = $row->fullname;
            $data['verification_code'] = $verification_code;
            $data['email'] = $row->email;
            $data['year'] = date('Y');

            $message_body = $this->parser->parse('auth/email_templates/verification_code', $data, TRUE);

            
            $this->db->where('id', $user_id);
            $this->db->update('aauth_users', array('verification_code' => $verification_code));

            $config['mailtype'] = 'html';
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'smtp.vianet.com.np';

            $this->email->initialize($config);

            $this->email->clear(TRUE);
            $this->email->from('no-reply@adidas.com','Adidas Store');
            $this->email->to($row->email);
            $this->email->subject('User verifition code.');
            $this->email->message($message_body);
            $this->email->send();
        }
    }

    public function verify_user_post(){

        $user_id = $this->input->post('user_id');
        $ver_code = $this->input->post('verification_code');

        $query = $this->db->where('id', $user_id);
        $query = $this->db->where('verification_code', $ver_code);
        $query = $this->db->get('aauth_users' );

        // if ver code is TRUE
        if( $query->num_rows() > 0 ){

            $data =  array(
                'verification_code' => '',
                'banned' => 0
            );

            $this->db->where('id', $user_id);
            $this->db->update('aauth_users' , $data);
            return TRUE;
        }
        return FALSE;
    }

    public function  login_email_post()
    {
        $email = $this->input->post('email');
        $pass  = $this->input->post('pass');

            // See if a user exists with the given credentials
        $result = $this->aauth->login($email, $pass);

        if ( $result )
        {
            $this->db->where('user_id',$result);
            $data  = $this->db->get('view_customer_details')->row();            
            $success = TRUE;
        }
        else
        {
            $data = NULL;
            $success = FALSE;
        }

        echo json_encode(array('success'=>$success,'rows'=>$data));
    }
}