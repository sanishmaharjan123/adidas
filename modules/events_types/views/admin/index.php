<!-- Content Wrapper. Contains page content -->
<section id="main-content">
	<section class="wrapper site-min-height">
		<h3><i class="fa fa-angle-right"></i><?php echo lang('events_types'); ?></h3>
		<div class="row mt">
			<div class="col-lg-12">
				<?php echo displayStatus(); ?>
				<div id='jqxGridEvents_typeToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Events_typemodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridEvents_typeFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridEvents_type"></div>
			</div>
		</div>
	</section>
</section>

<div id="Events_typemodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('events_types'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-Events_types', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "Events_types_id"/>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='events_types_name'><?php echo lang('name')?><span class='mandatory'>*</span></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='events_types_name' class=' form-control' name='name'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='description'><?php echo lang('description')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><textarea id='description' class='form-control' name='description' rows='6'></textarea></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxEvents_typeSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var Events_typesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'name', type: 'string' },
			{ name: 'description', type: 'string' },
			
			],
			url: '<?php echo site_url("admin/Events_types/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	Events_typesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridEvents_type").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridEvents_type").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridEvents_type").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: Events_typesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridEvents_typeToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editEvents_typeRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("description"); ?>',datafield: 'description',width: 650,filterable: true,renderer: gridColumnsRenderer },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridEvents_type").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridEvents_typeFilterClear', function () { 
		$('#jqxGridEvents_type').jqxGrid('clearfilters');
	});

	
    $('#form-Events_types').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#events_types_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#events_types_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},
        ]
    });

    $("#jqxEvents_typeSubmitButton").on('click', function () {
        var validationResult = function (isValid) {
                if (isValid) {
                   saveEvents_typeRecord();
                }
            };
        $('#form-Events_types').jqxValidator('validate', validationResult);
        
    });
});

function editEvents_typeRecord(index){
	var row =  $("#jqxGridEvents_type").jqxGrid('getrowdata', index);
	if (row) {
		$('#Events_types_id').val(row.id);
		$('#events_types_name').val(row.name);
		$('#description').val(row.description);
		
		$('#Events_typemodal').modal('show');
	}
}

function saveEvents_typeRecord(){
	var data = $("#form-Events_types").serialize();

	$('#Events_typemodal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Events_types/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_Events_types();
				$('#jqxGridEvents_type').jqxGrid('updatebounddata');
				$('#Events_typemodal').modal('hide');
			}
			$('#Events_typemodal').unblock();
		}
	});
}

function reset_form_Events_types(){
	$('#Events_types_id').val('');
	$('#form-Events_types')[0].reset();
}
</script>