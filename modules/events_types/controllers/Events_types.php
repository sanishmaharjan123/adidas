<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Events_types
 *
 * Extends the Public_Controller class
 * 
 */

class Events_types extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Events Types');

        $this->load->model('events_types/events_type_model');
        $this->lang->load('events_types/events_type');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('events_types');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'events_types';
		$this->load->view($this->_container,$data);
	}
}