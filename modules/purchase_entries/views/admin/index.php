<!-- Content Wrapper. Contains page content -->
<section id="main-content">
	<section class="wrapper site-min-height">
		<h3><i class="fa fa-angle-right"></i><?php echo lang('purchase_entries'); ?></h3>
		<div class="row mt">
			<div class="col-lg-12">
				<?php echo displayStatus(); ?>
				<div id='jqxGridPurchase_entryToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Purchase_entrymodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridPurchase_entryFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridPurchase_entry"></div>
			</div>
		</div>
	</section>
</section>

<div id="Purchase_entrymodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('purchase_entries'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-Purchase_entries', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "Purchase_entries_id"/>					
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='store_id'><?php echo lang('store_id')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><div id='store_id' class=' form-control' name='store_id'></div></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='receipt_number'><?php echo lang('receipt_number')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='receipt_number' class=' form-control' name='receipt_number'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='amount'><?php echo lang('amount')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='amount' class=' form-control' name='amount'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='receipt_image'><?php echo lang('receipt_image')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='receipt_image' class=' form-control' name='receipt_image'></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxPurchase_entrySubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<div id="Approvemodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Approve <?php echo lang('purchase_entries'); ?></h4>
			</div>
			<div class="modal-body ">
				<div class="form_fields_area col-md-offset-4">
						<?php echo form_open('', array('id' =>'form-Purchase_approve', 'onsubmit' => 'return false')); ?>
						<input type = "hidden" name = "id" id = "Purchase_approve_id"/>
						<input type = "hidden" name = "user_id" id = "approve_user_id"/>
						<input type = "hidden" name = "approval_receipt_number_post" id = "approval_receipt_number_post"/>
						<input type = "hidden" name = "approve_amount_post" id = "approve_amount_post"/>

						<div class='row form-group'>
							<div class='col-sm-8 col-md-10 col-lg-10' id = "approve_image_url">
								
							</div>
						</div>
						<div class="row form-group">
							<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='store_id'><?php echo lang('store_id')?></label>
							<div class="col-sm-8 col-md-10 col-lg-10" id="approve_store_id">
							</div>
						</div>
						<div class='row form-group'>
							<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='receipt_number'><?php echo lang('receipt_number')?></label>
							<div class='col-sm-8 col-md-10 col-lg-10' id='approve_receipt_number'></div>
						</div>
						<div class='row form-group'>
							<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='amount'><?php echo lang('amount')?></label>
							<div class='col-sm-8 col-md-10 col-lg-10' id='approve_amount'></div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2 col-md-2 col-lg-2"><input type="radio" name="approval" value="1"> Approve</div>
							<div class="col-sm-2 col-md-2 col-lg-2"><input type="radio" name="approval" value="0"> Reject</div>
						</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxPurchase_approveSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>


<div id="Viewmodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">View <?php echo lang('purchase_entries'); ?></h4>
			</div>
			<div class="modal-body ">
				<div class="form_fields_area col-md-offset-4">					

						<div class='row form-group'>
							<div class='col-sm-8 col-md-10 col-lg-10' id = "view_image_url">
								
							</div>
						</div>
						<div class="row form-group">
							<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='store_id'><?php echo lang('store_id')?></label>
							<div class="col-sm-8 col-md-10 col-lg-10" id="view_store_id">
							</div>
						</div>
						<div class='row form-group'>
							<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='receipt_number'><?php echo lang('receipt_number')?></label>
							<div class='col-sm-8 col-md-10 col-lg-10' id='view_receipt_number'></div>
						</div>
						<div class='row form-group'>
							<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='amount'><?php echo lang('amount')?></label>
							<div class='col-sm-8 col-md-10 col-lg-10' id='view_amount'></div>
						</div>						
				</div>
			</div>
		</div>
	</div>
</div>



<script language="javascript" type="text/javascript">

	$(function(){

		var Purchase_entriesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'user_id', type: 'number' },
			{ name: 'store_id', type: 'number' },
			{ name: 'receipt_number', type: 'string' },
			{ name: 'amount', type: 'string' },
			{ name: 'receipt_image', type: 'string' },
			{ name: 'date', type: 'date' },
			{ name: 'date_np', type: 'string' },
			{ name: 'image_url', type: 'string' },
			{ name: 'approved', type: 'number' },
			
			],
			url: '<?php echo site_url("admin/Purchase_entries/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	Purchase_entriesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridPurchase_entry").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridPurchase_entry").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridPurchase_entry").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: Purchase_entriesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridPurchase_entryToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index,column,value,a,c,d) {
				var e = '';				
				 // e +='<a href="javascript:void(0)" onclick="editPurchase_entryRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>&nbsp';
				 e +='<a href="javascript:void(0)" onclick="view_image(' + index + '); return false;" title="View Receipt"><i class="fa fa-eye"></i></a>&nbsp';
				
				if(d.approved==0){
					 e +='<a href="javascript:void(0)" onclick="open_approve(' + index + '); return false;" title="Approve"><i class="fa fa-check"></i></a>&nbsp';
				 }
				 return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			{ text: '<?php echo lang("user_id"); ?>',datafield: 'user_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("store_id"); ?>',datafield: 'store_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("receipt_number"); ?>',datafield: 'receipt_number',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("amount"); ?>',datafield: 'amount',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("date"); ?>',datafield: 'date',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("date_np"); ?>',datafield: 'date_np',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("image_url"); ?>',datafield: 'image_url',width: 150,filterable: true,renderer: gridColumnsRenderer, hidden: true, },
			],
			rendergridrows: function (result) {
				return result.data;
			}
		});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridPurchase_entry").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridPurchase_entryFilterClear', function () { 
		$('#jqxGridPurchase_entry').jqxGrid('clearfilters');
	});

	
	$('#form-Purchase_approve').jqxValidator({
		hintType: 'label',
		animationDuration: 500,
		rules: [
		{ input: '.approval', message: 'Required', action: 'blur', 
		rule: function(input) {
			val = $('.approval').val();
			return (val == '' || val == null) ? false: true;
		}
	},	] });

	$("#jqxPurchase_entrySubmitButton").on('click', function () {
		savePurchase_entryRecord();
    });

	
});
$("#jqxPurchase_approveSubmitButton").on('click', function () {
		savePurchase_approveRecord();
	});

function editPurchase_entryRecord(index){
	var row =  $("#jqxGridPurchase_entry").jqxGrid('getrowdata', index);
	if (row) {
		$('#Purchase_entries_id').val(row.id);
		$('#user_id').jqxNumberInput('val', row.user_id);
		$('#store_id').jqxComboBox('val', row.store_id);
		$('#receipt_number').val(row.receipt_number);
		$('#amount').val(rowapprove_user_id);
		$('#amount').val(row.amount);
		$('#date').jqxDateTimeInput('setDate', row.date);
		$('#date_np').val(row.date_np);		
		
		$('#Purchase_entrymodal').modal('show');
	}
}

function open_approve(index){
	var row =  $("#jqxGridPurchase_entry").jqxGrid('getrowdata', index);
	if (row) {
		var image_url = row.image_url;
		$('#Purchase_approve_id').val(row.id);
		$('#approve_user_id').val(row.user_id);
		$('#approve_store_id').append(row.store_id);
		$('#approve_receipt_number').append(row.receipt_number);
		$('#approve_amount').append(row.amount);
		$('#approve_amount_post').val(row.amount);
		$('#approve_image_url').html('<img src='+image_url+'>');
		$('#approval_receipt_number_post').val(row.receipt_number);

		$('#Approvemodal').modal('show');
	}
}

function view_image(index){
	var row =  $("#jqxGridPurchase_entry").jqxGrid('getrowdata', index);
	if (row) {
		var image_url = row.image_url;
		$('#view_store_id').append(row.store_id);
		$('#view_receipt_number').append(row.receipt_number);
		$('#view_amount').append(row.amount);
		$('#view_image_url').html('<img src='+image_url+'>');

		$('#Viewmodal').modal('show');
	}
}

function savePurchase_entryRecord(){
	var data = $("#form-Purchase_entries").serialize();

	$('#Purchase_entrymodal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Purchase_entries/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_Purchase_entries();
				$('#jqxGridPurchase_entry').jqxGrid('updatebounddata');
				$('#Purchase_entrymodal').modal('hide');
			}
			$('#Purchase_entrymodal').unblock();
		}
	});
}

function reset_form_Purchase_entries(){
	$('#Purchase_entries_id').val('');
	$('#form-Purchase_entries')[0].reset();
}

function savePurchase_approveRecord(){
	// alert ('here');
	var data = $("#form-Purchase_approve").serialize();

	$('#Approvemodal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Purchase_entries/approve_entry"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			// console.log(result);
			if (result) {
				reset_form_Purchase_entries();
				$('#jqxGridPurchase_entry').jqxGrid('updatebounddata');
				$('#Approvemodal').modal('hide');
			}
			$('#Approvemodal').unblock();
		}
	});
}


</script>