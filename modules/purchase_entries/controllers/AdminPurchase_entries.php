<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Purchase_entries
 *
 * Extends the Project_Controller class
 * 
 */

class AdminPurchase_entries extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		control('Purchase Entries');

		$this->load->model('purchase_entries/purchase_entry_model');
		$this->lang->load('purchase_entries/purchase_entry');
	}

	public function index()
	{
		// Display Page
		$data['header'] = lang('purchase_entries');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'purchase_entries';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->purchase_entry_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->purchase_entry_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
        	$success=$this->purchase_entry_model->insert($data);
        }
        else
        {
        	$success=$this->purchase_entry_model->update($data['id'],$data);
        }

        if($success)
        {
        	$success = TRUE;
        	$msg=lang('general_success');
        }
        else
        {
        	$success = FALSE;
        	$msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
    	$data=array();
    	if($this->input->post('id')) {
    		$data['id'] = $this->input->post('id');
    	}
    	$data['user_id'] = $this->session->userdata('id');
    	$data['store_id'] = $this->input->post('store_id');
    	$data['receipt_number'] = $this->input->post('receipt_number');
    	$data['amount'] = $this->input->post('amount');
    	$data['receipt_image'] = $this->input->post('receipt_image');
    	$data['date'] = date('Y-m-d');
    	$data['date_np'] = get_nepali_date(date('Y-m-d'),'nep');

    	return $data;
    }

    public function approve_entry()
    {
        // $date = date('Y-m-d');
        // echo "<pre>";
        // print_r($date);
        // exit;
        $success = false;
        $points = array();
    	$receipt_number = $this->input->post('approval_receipt_number_post');
    	if($this->input->post('approval'))
    	{
    		$data['approved'] = 1;
            $amount = $this->input->post('approve_amount_post');            
            $points['user_id'] = $this->input->post('user_id');
            $points['points'] = $amount/100;
            $points['date'] = date('Y-m-d');
            $points['receipt_number'] = $this->input->post('approval_receipt_number_post');            
    	}
    	else
    	{
    		$data['approved'] = 0;
    	} 
        $this->db->where('receipt_number', $receipt_number);
        $approval_success = $this->db->update('tbl_purchase_entries', $data);
        if($points){            
            $points_success = $this->db->insert('tbl_points', $points);
        }
        if($approval_success){
            $success = true;
        }
        echo json_encode('success',true);
    }
}