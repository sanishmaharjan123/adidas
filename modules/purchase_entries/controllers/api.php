<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{
	public function __construct()
    {
    	parent::__construct();
        $this->load->model('purchase_entries/purchase_entry_model');
    }

    public function purchase_entry_list_post()
    {
    	$user_id = $this->input->post('user_id');
        $this->purchase_entry_model->_table = "view_purchase_entries";
        $rows = $this->purchase_entry_model->findAll(array('user_id'=>$user_id));
        echo json_encode(array('rows'=>$rows));
    }

    public function purchase_entry_create_post()
    {
        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/adidas/uploads/purchase_entries/';
        $data['user_id'] = $this->input->post('user_id');
        $data['store_id'] = $this->input->post('store_id');
        $data['receipt_number'] = $this->input->post('receipt_number');
        $data['amount'] = $this->input->post('amount');
        $data['date'] = date('Y-m-d');
        $data['image_url'] = $upload_path.$data['receipt_number'].'.jpg';
        $success = $this->db->insert('tbl_purchase_entries',$data);

        if($success)
        {
            file_put_contents($data['image_url'], base64_decode($this->input->post('image')));
            $success = TRUE;
        }
        else
        {
            $success = FALSE;
        }

        echo json_encode(array('success'=>$success));
    }

    public function purchase_entry_delete()
    {
	    //TODO
    }


}