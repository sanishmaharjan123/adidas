<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Purchase_entries
 *
 * Extends the Public_Controller class
 * 
 */

class Purchase_entries extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Purchase Entries');

        $this->load->model('purchase_entries/purchase_entry_model');
        $this->lang->load('purchase_entries/purchase_entry');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('purchase_entries');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'purchase_entries';
		$this->load->view($this->_container,$data);
	}
}