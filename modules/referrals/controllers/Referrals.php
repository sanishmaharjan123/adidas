<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Referrals
 *
 * Extends the Public_Controller class
 * 
 */

class Referrals extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Referrals');

        $this->load->model('referrals/referral_model');
        $this->lang->load('referrals/referral');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('referrals');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'referrals';
		$this->load->view($this->_container,$data);
	}
}