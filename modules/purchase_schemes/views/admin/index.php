<!-- Content Wrapper. Contains page content -->
<section id="main-content">
	<section class="wrapper site-min-height">
		<h3><i class="fa fa-angle-right"></i><?php echo lang('purchase_schemes'); ?></h3>
		<div class="row mt">
			<div class="col-lg-12">
				<?php echo displayStatus(); ?>
				<div id='jqxGridPurchase_schemeToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Purchase_schememodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridPurchase_schemeFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridPurchase_scheme"></div>
			</div>
		</div>
	</section>
</section>

<div id="Purchase_schememodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('purchase_schemes'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-Purchase_schemes', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "Purchase_schemes_id"/>
					
				<div class='row form-group'>
					<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='created_by'><?php echo lang('created_by')?></label>
					<div class='col-sm-8 col-md-10 col-lg-10'><div id='created_by' class=' form-control' name='created_by'></div></div>
				</div>
				<div class='row form-group'>
					<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='updated_by'><?php echo lang('updated_by')?></label>
					<div class='col-sm-8 col-md-10 col-lg-10'><div id='updated_by' class=' form-control' name='updated_by'></div></div>
				</div>
				<div class='row form-group'>
					<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='deleted_by'><?php echo lang('deleted_by')?></label>
					<div class='col-sm-8 col-md-10 col-lg-10'><div id='deleted_by' class=' form-control' name='deleted_by'></div></div>
				</div>
				<div class='row form-group'>
					<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='created_at'><?php echo lang('created_at')?></label>
					<div class='col-sm-8 col-md-10 col-lg-10'><input id='created_at' class=' form-control' name='created_at'></div>
				</div>
				<div class='row form-group'>
					<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='updated_at'><?php echo lang('updated_at')?></label>
					<div class='col-sm-8 col-md-10 col-lg-10'><input id='updated_at' class=' form-control' name='updated_at'></div>
				</div>
				<div class='row form-group'>
					<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='deleted_at'><?php echo lang('deleted_at')?></label>
					<div class='col-sm-8 col-md-10 col-lg-10'><input id='deleted_at' class=' form-control' name='deleted_at'></div>
				</div>
				<div class='row form-group'>
					<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='store_id'><?php echo lang('store_id')?></label>
					<div class='col-sm-8 col-md-10 col-lg-10'><div id='store_id' class=' form-control' name='store_id'></div></div>
				</div>
				<div class='row form-group'>
					<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='start_amount'><?php echo lang('start_amount')?></label>
					<div class='col-sm-8 col-md-10 col-lg-10'><input id='start_amount' class=' form-control' name='start_amount'></div>
				</div>
				<div class='row form-group'>
					<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='end_amount'><?php echo lang('end_amount')?></label>
					<div class='col-sm-8 col-md-10 col-lg-10'><input id='end_amount' class=' form-control' name='end_amount'></div>
				</div>
				<div class='row form-group'>
					<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='notification'><?php echo lang('notification')?></label>
					<div class='col-sm-8 col-md-10 col-lg-10'><input id='notification' class=' form-control' name='notification'></div>
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxPurchase_schemeSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var Purchase_schemesDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'store_id', type: 'number' },
			{ name: 'start_amount', type: 'string' },
			{ name: 'end_amount', type: 'string' },
			{ name: 'notification', type: 'string' },
			
			],
			url: '<?php echo site_url("admin/Purchase_schemes/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	Purchase_schemesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridPurchase_scheme").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridPurchase_scheme").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridPurchase_scheme").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: Purchase_schemesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridPurchase_schemeToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editPurchase_schemeRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("store_id"); ?>',datafield: 'store_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("start_amount"); ?>',datafield: 'start_amount',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("end_amount"); ?>',datafield: 'end_amount',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("notification"); ?>',datafield: 'notification',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridPurchase_scheme").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridPurchase_schemeFilterClear', function () { 
		$('#jqxGridPurchase_scheme').jqxGrid('clearfilters');
	});

	
    /*$('#form-Purchase_schemes').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#store_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#store_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#start_amount', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#start_amount').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#end_amount', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#end_amount').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#notification', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#notification').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxPurchase_schemeSubmitButton").on('click', function () {
    	savePurchase_schemeRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   savePurchase_schemeRecord();
                }
            };
        $('#form-Purchase_schemes').jqxValidator('validate', validationResult);
        */
    });
});

	function editPurchase_schemeRecord(index){
		var row =  $("#jqxGridPurchase_scheme").jqxGrid('getrowdata', index);
		if (row) {
			$('#Purchase_schemes_id').val(row.id);
			$('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_by').jqxNumberInput('val', row.deleted_by);
		$('#created_at').val(row.created_at);
		$('#updated_at').val(row.updated_at);
		$('#deleted_at').val(row.deleted_at);
		$('#store_id').jqxNumberInput('val', row.store_id);
		$('#start_amount').val(row.start_amount);
		$('#end_amount').val(row.end_amount);
		$('#notification').val(row.notification);
		
			$('#Purchase_schememodal').modal('show');
		}
	}

	function savePurchase_schemeRecord(){
		var data = $("#form-Purchase_schemes").serialize();

		$('#Purchase_schememodal').block({ 
			message: '<span>Processing your request. Please be patient.</span>',
			css: { 
				width                   : '75%',
				border                  : 'none', 
				padding                 : '50px', 
				backgroundColor         : '#000', 
				'-webkit-border-radius' : '10px', 
				'-moz-border-radius'    : '10px', 
				opacity                 : .7, 
				color                   : '#fff',
				cursor                  : 'wait' 
			}, 
		});

		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/Purchase_schemes/save"); ?>',
			data: data,
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {
					reset_form_Purchase_schemes();
					$('#jqxGridPurchase_scheme').jqxGrid('updatebounddata');
					$('#Purchase_schememodal').modal('hide');
				}
				$('#Purchase_schememodal').unblock();
			}
		});
	}

	function reset_form_Purchase_schemes(){
		$('#Purchase_schemes_id').val('');
		$('#form-Purchase_schemes')[0].reset();
	}
</script>