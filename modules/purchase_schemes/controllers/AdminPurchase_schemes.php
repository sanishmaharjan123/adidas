<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Purchase_schemes
 *
 * Extends the Project_Controller class
 * 
 */

class AdminPurchase_schemes extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Purchase Schemes');

        $this->load->model('purchase_schemes/purchase_scheme_model');
        $this->lang->load('purchase_schemes/purchase_scheme');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('purchase_schemes');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'purchase_schemes';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->purchase_scheme_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->purchase_scheme_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->purchase_scheme_model->insert($data);
        }
        else
        {
            $success=$this->purchase_scheme_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['store_id'] = $this->input->post('store_id');
		$data['start_amount'] = $this->input->post('start_amount');
		$data['end_amount'] = $this->input->post('end_amount');
		$data['notification'] = $this->input->post('notification');

        return $data;
   }
}