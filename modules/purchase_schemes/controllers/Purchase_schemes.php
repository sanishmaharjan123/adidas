<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Purchase_schemes
 *
 * Extends the Public_Controller class
 * 
 */

class Purchase_schemes extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Purchase Schemes');

        $this->load->model('purchase_schemes/purchase_scheme_model');
        $this->lang->load('purchase_schemes/purchase_scheme');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('purchase_schemes');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'purchase_schemes';
		$this->load->view($this->_container,$data);
	}
}