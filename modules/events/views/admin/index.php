<!-- Content Wrapper. Contains page content -->
<section id="main-content">
	<section class="wrapper site-min-height">
		<h3><i class="fa fa-angle-right"></i><?php echo lang('events'); ?></h3>
		<div class="row mt">
			<div class="col-lg-12">
				<?php echo displayStatus(); ?>
				<div id='jqxGridEventToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Eventmodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridEventFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridEvent"></div>
			</div>
		</div>
	</section>
</section>

<div id="Eventmodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('events'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-Events', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "Events_id"/>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='event_type_id'><?php echo lang('event_type_id')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><div id='event_type_id' class='form-control' name='event_type_id'></div></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='store_id'><?php echo lang('store_id')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><div id='store_id' class=' form-control' name='store_id'></div></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='event_date'><?php echo lang('event_date')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><div id='event_date' class=' form-control' name='event_date'></div></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='no_of_days'><?php echo lang('no_of_days')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input type = "number" id='no_of_days' class='form-control' name='no_of_days'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='title'><?php echo lang('title')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='title' class=' form-control' name='title'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='message'><?php echo lang('message')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><textarea id='message' class=' form-control' name='message' rows = "10"></textarea></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxEventSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		$("#event_date").jqxDateTimeInput({ width:'73%', height: '25px', formatString: formatString_yyyy_MM_dd });

		var event_typeDataSource = {
			url : '<?php echo site_url("admin/Events/get_event_type_combo_json"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'name', type: 'string' },
			],
			async: false,
			cache: true
		}

		event_typeDataAdapter = new $.jqx.dataAdapter(event_typeDataSource);

		$("#event_type_id").jqxComboBox({
			theme: theme,
			width: '96%',
			height: 30,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: event_typeDataAdapter,
			displayMember: "name",
			valueMember: "id",
		});

		var storeDataSource = {
			url : '<?php echo site_url("admin/Events/get_stores_combo_json"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'name', type: 'string' },
			],
			async: false,
			cache: true
		}

		storeDataAdapter = new $.jqx.dataAdapter(storeDataSource);

		$("#store_id").jqxComboBox({
			theme: theme,
			width: '96%',
			height: 30,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: storeDataAdapter,
			displayMember: "name",
			valueMember: "id",
		});

		var EventsDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'store_id', type: 'number' },
			{ name: 'event_date', type: 'date' },
			{ name: 'event_date_np', type: 'string' },
			{ name: 'no_of_days', type: 'number' },
			{ name: 'message', type: 'string' },
			{ name: 'event_type_id', type: 'number' },
			{ name: 'event_type', type: 'string' },
			{ name: 'title', type: 'string' },
			
			],
			url: '<?php echo site_url("admin/Events/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	EventsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridEvent").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridEvent").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridEvent").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: EventsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridEventToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editEventRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("store_id"); ?>',datafield: 'store_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("event_date"); ?>',datafield: 'event_date',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
		{ text: '<?php echo lang("no_of_days"); ?>',datafield: 'no_of_days',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("title"); ?>',datafield: 'title',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("message"); ?>',datafield: 'message',width: 150,filterable: true,renderer: gridColumnsRenderer },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridEvent").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridEventFilterClear', function () { 
		$('#jqxGridEvent').jqxGrid('clearfilters');
	});

	
    $('#form-Events').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#event_type_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#event_type_id').jqxComboBox('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},
			{ input: '#store_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#store_id').jqxComboBox('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},
			{ input: '#title', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#title').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},
			{ input: '#message', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#message').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });

    $("#jqxEventSubmitButton").on('click', function () {
        
        var validationResult = function (isValid) {
                if (isValid) {
                   saveEventRecord();
                }
            };
        $('#form-Events').jqxValidator('validate', validationResult);
        
    });
});

function editEventRecord(index){
	var row =  $("#jqxGridEvent").jqxGrid('getrowdata', index);
	if (row) {
		$('#Events_id').val(row.id);
		$('#event_type_id').jqxComboBox('val', row.event_type_id);
		$('#store_id').jqxComboBox('val', row.store_id);
		$('#event_date').jqxDateTimeInput('setDate', row.event_date);
		$('#event_date_np').val(row.event_date_np);
		$('#no_of_days').jqxNumberInput('val', row.no_of_days);
		$('#title').val(row.message);
		$('#message').val(row.message);
		
		$('#Eventmodal').modal('show');
	}
}

function saveEventRecord(){
	var data = $("#form-Events").serialize();

	$('#Eventmodal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Events/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_Events();
				$('#jqxGridEvent').jqxGrid('updatebounddata');
				$('#Eventmodal').modal('hide');
			}
			$('#Eventmodal').unblock();
		}
	});
}

function reset_form_Events(){
	$('#Events_id').val('');
	$('#form-Events')[0].reset();
}
</script>