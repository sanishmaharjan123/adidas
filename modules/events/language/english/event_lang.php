<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['event_type_id'] = 'Event Type';
$lang['store_id'] = 'Store';
$lang['event_date'] = 'Event Date';
$lang['event_date_np'] = 'Event Date Np';
$lang['no_of_days'] = 'No Of Days';
$lang['title'] = 'Title';
$lang['message'] = 'Message';

$lang['events']='Events';