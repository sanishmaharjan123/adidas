<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Customer_details
 *
 * Extends the Project_Controller class
 * 
 */

class AdminCustomer_details extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Customer Details');

        $this->load->model('customer_details/customer_detail_model');
        $this->lang->load('customer_details/customer_detail');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('customer_details');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'customer_details';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->customer_detail_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->customer_detail_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->customer_detail_model->insert($data);
        }
        else
        {
            $success=$this->customer_detail_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['user_id'] = $this->input->post('user_id');
		$data['address'] = $this->input->post('address');
		$data['mobile'] = $this->input->post('mobile');
		$data['email'] = $this->input->post('email');
		$data['dob'] = $this->input->post('dob');
		$data['anniversary'] = $this->input->post('anniversary');

        return $data;
   }
}