<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Customer_details
 *
 * Extends the Public_Controller class
 * 
 */

class Customer_details extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Customer Details');

        $this->load->model('customer_details/customer_detail_model');
        $this->lang->load('customer_details/customer_detail');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('customer_details');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'customer_details';
		$this->load->view($this->_container,$data);
	}
}