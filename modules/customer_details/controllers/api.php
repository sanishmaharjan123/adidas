<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{
	public function __construct()
    {
    	parent::__construct();
        $this->load->model('customer_details/customer_detail_model');
    }

    public function customer_detail_post()
    {
        $user_id = $this->input->post('user_id');
        $this->customer_detail_model->_table = "view_customer_details";
        $rows = $this->customer_detail_model->find(array('user_id'=>$user_id));

        echo json_encode(array('rows'=>$rows));
    }

    public function edit_customer_detail_post()
    {
        $data['id'] = $this->input->post('customer_id');
        $data['mobile'] = $this->input->post('mobile');
        $data['address'] = $this->input->post('address');
        $data['dob'] = $this->input->post('dob');
        $data['anniversary'] = $this->input->post('anniversary');

        $this->db->where('id',$data['id']);
        $result = $this->db->update('tbl_customer_details',$data);

        if($result)
        {
            $success = TRUE;
            $rows = $data;
        }
        else
        {
            $success = FALSE;
            $rows = NULL;
        }

        echo json_encode(array('success'=>$success,'rows'=>$rows));
    }


    public function customer_detail_delete()
    {
	    //TODO
    }


    public function customer_birthday_post()
    {
        $id = $this->input->post('user_id');
        $event = $this->input->post('event');
        $this->db->where('id',$id); 
        $detail = $this->db->get('view_users')->result();
        $this->db->where('title',$event);
        $offer = $this->db->get('tbl_events')->result();
        if($detail){
            if($offer){
                $data = array(
                    'user_id' => $this->input->post('user_id'),
                    'notification' =>'Happy '.$offer[0]->title.' '.$detail[0]->fullname.'! Visit our store and get a '.$offer[0]->message.' as a Birthday Gift from Adidas.',
                    'title' => $offer[0]->title,
                    'created_by' => 2,                    
                    'created_at' => date('Y-m-d H:i:s')
                );  
                $this->db->insert('tbl_notification_log', $data);
                echo json_encode(array('title'=>'Happy '.$offer[0]->title.' '.$detail[0]->fullname, 'notification'=>'Happy '.$offer[0]->title.' '.$detail[0]->fullname.'! Visit our store and get a '.$offer[0]->message.' as a Birthday Gift from Adidas.'));                
            }
            else{
                echo json_encode(array('error'=>true,'message'=>'Sorry! No offer found'));                
            }
        }
        else{
            echo json_encode(array('error'=>true,'message'=>'Sorry! No data found'));
        }
    }
    


}