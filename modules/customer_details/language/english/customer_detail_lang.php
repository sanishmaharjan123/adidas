<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['user_id'] = 'User Id';
$lang['address'] = 'Address';
$lang['mobile'] = 'Mobile';
$lang['email'] = 'Email';
$lang['dob'] = 'Dob';
$lang['anniversary'] = 'Anniversary';

$lang['customer_details']='Customer Details';