<!-- Content Wrapper. Contains page content -->
<section id="main-content">
	<section class="wrapper site-min-height">
		<h3><i class="fa fa-angle-right"></i><?php echo lang('customer_details'); ?></h3>
		<div class="row mt">
			<div class="col-lg-12">
				<?php echo displayStatus(); ?>
				<div id='jqxGridCustomer_detailToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Customer_detailmodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridCustomer_detailFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridCustomer_detail"></div>
			</div>
		</div>
	</section>
</section>

<div id="Customer_detailmodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('customer_details'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-Customer_details', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "Customer_details_id"/>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='user_id'><?php echo lang('user_id')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><div id='user_id' class=' form-control' name='user_id'></div></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='address'><?php echo lang('address')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='address' class=' form-control' name='address'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='mobile'><?php echo lang('mobile')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='mobile' class=' form-control' name='mobile'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='email'><?php echo lang('email')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='email' class=' form-control' name='email'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='dob'><?php echo lang('dob')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><div id='dob' class=' form-control' name='dob'></div></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='anniversary'><?php echo lang('anniversary')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><div id='anniversary' class=' form-control' name='anniversary'></div></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxCustomer_detailSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var Customer_detailsDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'user_id', type: 'number' },
			{ name: 'address', type: 'string' },
			{ name: 'mobile', type: 'string' },
			{ name: 'email', type: 'string' },
			{ name: 'dob', type: 'date' },
			{ name: 'anniversary', type: 'date' },
			
			],
			url: '<?php echo site_url("admin/Customer_details/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	Customer_detailsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridCustomer_detail").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridCustomer_detail").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridCustomer_detail").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: Customer_detailsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridCustomer_detailToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editCustomer_detailRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("user_id"); ?>',datafield: 'user_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("address"); ?>',datafield: 'address',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("mobile"); ?>',datafield: 'mobile',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("email"); ?>',datafield: 'email',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("dob"); ?>',datafield: 'dob',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
		{ text: '<?php echo lang("anniversary"); ?>',datafield: 'anniversary',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridCustomer_detail").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridCustomer_detailFilterClear', function () { 
		$('#jqxGridCustomer_detail').jqxGrid('clearfilters');
	});

	
    /*$('#form-Customer_details').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#user_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#user_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#address', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#address').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#mobile', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#mobile').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#email', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#email').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxCustomer_detailSubmitButton").on('click', function () {
    	saveCustomer_detailRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveCustomer_detailRecord();
                }
            };
        $('#form-Customer_details').jqxValidator('validate', validationResult);
        */
    });
});

function editCustomer_detailRecord(index){
	var row =  $("#jqxGridCustomer_detail").jqxGrid('getrowdata', index);
	if (row) {
		$('#Customer_details_id').val(row.id);
		$('#user_id').jqxNumberInput('val', row.user_id);
		$('#address').val(row.address);
		$('#mobile').val(row.mobile);
		$('#email').val(row.email);
		$('#dob').jqxDateTimeInput('setDate', row.dob);
		$('#anniversary').jqxDateTimeInput('setDate', row.anniversary);
		
		$('#Customer_detailmodal').modal('show');
	}
}

function saveCustomer_detailRecord(){
	var data = $("#form-Customer_details").serialize();

	$('#Customer_detailmodal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Customer_details/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_Customer_details();
				$('#jqxGridCustomer_detail').jqxGrid('updatebounddata');
				$('#Customer_detailmodal').modal('hide');
			}
			$('#Customer_detailmodal').unblock();
		}
	});
}

function reset_form_Customer_details(){
	$('#Customer_details_id').val('');
	$('#form-Customer_details')[0].reset();
}
</script>