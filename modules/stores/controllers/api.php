<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{
	public function __construct()
    {
    	parent::__construct();
        $this->load->model('stores/store_model');
    }

    public function store_get()
    {
        $rows = $this->store_model->findAll();
        echo json_encode(array('rows'=>$rows));
    }

    public function store_post()
    {
	    //TODO
    }

    public function store_delete()
    {
	    //TODO
    }


}