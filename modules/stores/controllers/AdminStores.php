<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Stores
 *
 * Extends the Project_Controller class
 * 
 */

class AdminStores extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Stores');

        $this->load->model('stores/store_model');
        $this->lang->load('stores/store');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('stores');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'stores';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->store_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->store_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->store_model->insert($data);
        }
        else
        {
            $success=$this->store_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['name'] = $this->input->post('name');
		$data['address'] = $this->input->post('address');
		$data['mobile'] = $this->input->post('mobile');
		$data['phone'] = $this->input->post('phone');
		$data['email'] = $this->input->post('email');
		$data['incharge'] = $this->input->post('incharge');
		$data['latitide'] = $this->input->post('latitide');
		$data['longitude'] = $this->input->post('longitude');

        return $data;
   }
}