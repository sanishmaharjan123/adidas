<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Stores
 *
 * Extends the Public_Controller class
 * 
 */

class Stores extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Stores');

        $this->load->model('stores/store_model');
        $this->lang->load('stores/store');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('stores');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'stores';
		$this->load->view($this->_container,$data);
	}
}