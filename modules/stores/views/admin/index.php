<!-- Content Wrapper. Contains page content -->
<section id="main-content">
	<section class="wrapper site-min-height">
		<h3><i class="fa fa-angle-right"></i><?php echo lang('stores'); ?></h3>
		<div class="row mt">
			<div class="col-lg-12">
				<?php echo displayStatus(); ?>
				<div id='jqxGridStoreToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Storemodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridStoreFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridStore"></div>
			</div>
		</div>
	</section>
</section>

<div id="Storemodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('stores'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-Stores', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "Stores_id"/>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='stores_name'><?php echo lang('name')?><span class='mandatory'>*</span></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='stores_name' class=' form-control' name='name'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='address'><?php echo lang('address')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='address' class=' form-control' name='address'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='mobile'><?php echo lang('mobile')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='mobile' class=' form-control' name='mobile'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='phone'><?php echo lang('phone')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='phone' class=' form-control' name='phone'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='email'><?php echo lang('email')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='email' class=' form-control' name='email'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='incharge'><?php echo lang('incharge')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><div id='incharge' class=' form-control' name='incharge'></div></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='latitide'><?php echo lang('latitide')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='latitide' class=' form-control' name='latitide'></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='longitude'><?php echo lang('longitude')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='longitude' class=' form-control' name='longitude'></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxStoreSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var agentDataSource = {
			url : '<?php echo site_url("admin/Stores/get_store_incharge_combo_json"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'user_id', type: 'number' },
			{ name: 'fullname', type: 'string' },
			],
			async: false,
			cache: true
		}

		agentDataAdapter = new $.jqx.dataAdapter(agentDataSource);

		$("#incharge").jqxComboBox({
			theme: theme,
			width: '96%',
			height: 30,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: agentDataAdapter,
			displayMember: "fullname",
			valueMember: "user_id",
		});

		var StoresDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'name', type: 'string' },
			{ name: 'address', type: 'string' },
			{ name: 'mobile', type: 'string' },
			{ name: 'phone', type: 'string' },
			{ name: 'email', type: 'string' },
			{ name: 'incharge', type: 'number' },
			{ name: 'latitide', type: 'string' },
			{ name: 'longitude', type: 'string' },
			
			],
			url: '<?php echo site_url("admin/Stores/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	StoresDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridStore").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridStore").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridStore").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: StoresDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridStoreToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editStoreRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("address"); ?>',datafield: 'address',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("mobile"); ?>',datafield: 'mobile',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("phone"); ?>',datafield: 'phone',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("email"); ?>',datafield: 'email',width: 180,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("incharge"); ?>',datafield: 'incharge',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("latitide"); ?>',datafield: 'latitide',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("longitude"); ?>',datafield: 'longitude',width: 150,filterable: true,renderer: gridColumnsRenderer },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridStore").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridStoreFilterClear', function () { 
		$('#jqxGridStore').jqxGrid('clearfilters');
	});

	
    $('#form-Stores').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#stores_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#stores_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#address', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#address').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#mobile', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#mobile').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},
			{ input: '#latitide', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#latitide').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#longitude', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#longitude').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},
        ]
    });

    $("#jqxStoreSubmitButton").on('click', function () {
        
        var validationResult = function (isValid) {
                if (isValid) {
                   saveStoreRecord();
                }
            };
        $('#form-Stores').jqxValidator('validate', validationResult);
        
    });
});

function editStoreRecord(index){
	var row =  $("#jqxGridStore").jqxGrid('getrowdata', index);
	if (row) {
		$('#Stores_id').val(row.id);
		$('#stores_name').val(row.name);
		$('#address').val(row.address);
		$('#mobile').val(row.mobile);
		$('#phone').val(row.phone);
		$('#email').val(row.email);
		$('#incharge').jqxComboBox('val', row.incharge);
		$('#latitide').val(row.latitide);
		$('#longitude').val(row.longitude);
		
		$('#Storemodal').modal('show');
	}
}

function saveStoreRecord(){
	var data = $("#form-Stores").serialize();

	$('#Storemodal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Stores/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_Stores();
				$('#jqxGridStore').jqxGrid('updatebounddata');
				$('#Storemodal').modal('hide');
			}
			$('#Storemodal').unblock();
		}
	});
}

function reset_form_Stores(){
	$('#Stores_id').val('');
	$('#form-Stores')[0].reset();
}
</script>