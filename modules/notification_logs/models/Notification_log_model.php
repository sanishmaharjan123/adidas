<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


class Notification_log_model extends MY_Model
{

    protected $_table = 'tbl_notification_log';

    protected $blamable = TRUE;

}