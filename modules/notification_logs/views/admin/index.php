<!-- Content Wrapper. Contains page content -->
<section id="main-content">
	<section class="wrapper site-min-height">
		<h3><i class="fa fa-angle-right"></i><?php echo lang('notification_logs'); ?></h3>
		<div class="row mt">
			<div class="col-lg-12">
				<?php echo displayStatus(); ?>
				<div id='jqxGridNotification_logToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs"  id="jqxGridAgentInsert" data-toggle="modal" data-target="#Notification_logmodal">Create</button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridNotification_logFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridNotification_log"></div>
			</div>
		</div>
	</section>
</section>

<div id="Notification_logmodal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add <?php echo lang('notification_logs'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form_fields_area">
					<?php echo form_open('', array('id' =>'form-Notification_logs', 'onsubmit' => 'return false')); ?>
					<input type = "hidden" name = "id" id = "Notification_logs_id"/>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='user_id'><?php echo lang('user_id')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><div id='user_id' class=' form-control' name='user_id'></div></div>
					</div>
					<div class='row form-group'>
						<label class='col-sm-4 col-md-2 col-lg-2 control-label' for='notification'><?php echo lang('notification')?></label>
						<div class='col-sm-8 col-md-10 col-lg-10'><input id='notification' class=' form-control' name='notification'></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="jqxNotification_logSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('general_cancel'); ?></button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script language="javascript" type="text/javascript">

	$(function(){

		var Notification_logsDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'user_id', type: 'number' },
			{ name: 'notification', type: 'string' },
			
			],
			url: '<?php echo site_url("admin/Notification_logs/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	Notification_logsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridNotification_log").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridNotification_log").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridNotification_log").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: Notification_logsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridNotification_logToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editNotification_logRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},
		{ text: '<?php echo lang("user_id"); ?>',datafield: 'user_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("notification"); ?>',datafield: 'notification',width: 150,filterable: true,renderer: gridColumnsRenderer },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridNotification_log").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridNotification_logFilterClear', function () { 
		$('#jqxGridNotification_log').jqxGrid('clearfilters');
	});

	
    /*$('#form-Notification_logs').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#user_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#user_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#notification', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#notification').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxNotification_logSubmitButton").on('click', function () {
    	saveNotification_logRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveNotification_logRecord();
                }
            };
        $('#form-Notification_logs').jqxValidator('validate', validationResult);
        */
    });
});

function editNotification_logRecord(index){
	var row =  $("#jqxGridNotification_log").jqxGrid('getrowdata', index);
	if (row) {
		$('#Notification_logs_id').val(row.id);
		$('#user_id').jqxNumberInput('val', row.user_id);
		$('#notification').val(row.notification);
		
		$('#Notification_logmodal').modal('show');
	}
}

function saveNotification_logRecord(){
	var data = $("#form-Notification_logs").serialize();

	$('#Notification_logmodal').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/Notification_logs/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_Notification_logs();
				$('#jqxGridNotification_log').jqxGrid('updatebounddata');
				$('#Notification_logmodal').modal('hide');
			}
			$('#Notification_logmodal').unblock();
		}
	});
}

function reset_form_Notification_logs(){
	$('#Notification_logs_id').val('');
	$('#form-Notification_logs')[0].reset();
}
</script>