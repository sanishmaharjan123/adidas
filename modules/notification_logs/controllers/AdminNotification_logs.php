<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Notification_logs
 *
 * Extends the Project_Controller class
 * 
 */

class AdminNotification_logs extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Notification Logs');

        $this->load->model('notification_logs/notification_log_model');
        $this->lang->load('notification_logs/notification_log');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('notification_logs');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'notification_logs';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->notification_log_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->notification_log_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->notification_log_model->insert($data);
        }
        else
        {
            $success=$this->notification_log_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['user_id'] = $this->input->post('user_id');
		$data['notification'] = $this->input->post('notification');

        return $data;
   }
}