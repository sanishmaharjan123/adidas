<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Notification_logs
 *
 * Extends the Public_Controller class
 * 
 */

class Notification_logs extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Notification Logs');

        $this->load->model('notification_logs/notification_log_model');
        $this->lang->load('notification_logs/notification_log');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('notification_logs');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'notification_logs';
		$this->load->view($this->_container,$data);
	}
}