<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{
	public function __construct()
    {
    	parent::__construct();
        $this->load->model('notification_logs/notification_log_model');
    }

    public function notification_log_get()
    {
    	//TODO
    }

    public function notification_log_post()
    {
        $user_id = $this->input->post('user_id');
        $rows = $this->notification_log_model->findAll(array('user_id'=>$user_id));
        echo json_encode(array('rows'=>$rows));
    }

    public function notification_log_delete()
    {
	    //TODO
    }


}