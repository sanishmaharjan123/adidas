<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_tbl_customer_details
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_tbl_customer_details extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('tbl_customer_details'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',           'constraint' => 11,     'unsigned' => TRUE, 'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp',     'default'    => null),
                'updated_at'            => array('type' => 'timestamp',     'default'    => null),
                'deleted_at'            => array('type' => 'timestamp',     'default'    => null),
                'user_id'               => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'address'               => array('type' => 'varchar',       'constraint' => 255 ),
                'mobile'                => array('type' => 'varchar',       'constraint' => 255 ),
                'email'                 => array('type' => 'varchar',       'constraint' => 255 ),
                'dob'                   => array('type' => 'date' ),
                'anniversary'           => array('type' => 'date'),
             ));

            $this->dbforge->create_table('tbl_customer_details', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('tbl_customer_details');
    }
}