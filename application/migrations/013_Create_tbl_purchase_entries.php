<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_tbl_purchase_entries
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_tbl_purchase_entries extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('tbl_purchase_entries'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',           'constraint' => 11,     'unsigned' => TRUE, 'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp',     'default'    => null),
                'updated_at'            => array('type' => 'timestamp',     'default'    => null),
                'deleted_at'            => array('type' => 'timestamp',     'default'    => null),
                'user_id'               => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'store_id'              => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'receipt_number'        => array('type' => 'varchar',       'constraint' => 255 ),
                'amount'                => array('type' => 'float',         'constraint' => 32,     'null' => TRUE),
                'receipt_image'         => array('type' => 'varchar',       'constraint' => 255 ),
                'date'                  => array('type' => 'date'),
                'date_np'               => array('type' => 'varchar',       'constraint' => 255 ),
             ));

            $this->dbforge->create_table('tbl_purchase_entries', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('tbl_purchase_entries');
    }
}