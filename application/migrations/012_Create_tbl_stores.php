<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_tbl_stores
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_tbl_stores extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('tbl_stores'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',           'constraint' => 11,     'unsigned' => TRUE, 'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp',     'default'    => null),
                'updated_at'            => array('type' => 'timestamp',     'default'    => null),
                'deleted_at'            => array('type' => 'timestamp',     'default'    => null),
                'name'                  => array('type' => 'varchar',       'constraint' => 255, 'null' => FALSE ),
                'address'               => array('type' => 'varchar',       'constraint' => 255 ),
                'mobile'                => array('type' => 'varchar',       'constraint' => 255, 'null' => FALSE ),
                'phone'                 => array('type' => 'varchar',       'constraint' => 255 ),
                'email'                 => array('type' => 'varchar',       'constraint' => 255 ),
                'incharge'              => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'latitide'              => array('type' => 'varchar',       'constraint' => 255 ),
                'longitude'             => array('type' => 'varchar',       'constraint' => 255 ),

             ));

            $this->dbforge->create_table('tbl_stores', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('tbl_stores');
    }
}