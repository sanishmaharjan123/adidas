<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_tbl_events
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_tbl_events extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('tbl_events'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',           'constraint' => 11,     'unsigned' => TRUE, 'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp',     'default'    => null),
                'updated_at'            => array('type' => 'timestamp',     'default'    => null),
                'deleted_at'            => array('type' => 'timestamp',     'default'    => null),
                'store_id'              => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'event_date'            => array('type' => 'date',          'null' => TRUE ),
                'event_date_np'         => array('type' => 'varchar',       'constraint' => 255,    'null' => TRUE ),
                'no_of_days'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
                'message'               => array('type' => 'varchar',       'constraint' => 255,    'null' => TRUE ),
                'title'                 => array('type' => 'varchar',       'constraint' => 255,    'null' => TRUE ),
                'event_type'            => array('type' => 'int',           'constraint' => 11,     'null' => TRUE),
             ));

            $this->dbforge->create_table('tbl_events', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('tbl_events');
    }
}