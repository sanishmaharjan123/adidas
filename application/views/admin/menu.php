<?php $uri = explode("/", $this->uri->uri_string()); ?>
<aside>
  <div id="sidebar"  class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">

      <p class="centered"><a href="profile.html"><img src="assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
      <h5 class="centered">Marcel Newman</h5>

      <li class="mt">
        <a class="active" href="<?php echo site_url();?>">
          <i class="fa fa-dashboard"></i>
          <span>Dashboard</span>
        </a>
      </li>

      <?php if(control('System', FALSE)):?>
        <li class="sub-menu">
          <a href="javascript:;" >
            <i class="fa fa-desktop"></i>
            <span>System</span>
          </a>
          <ul class="sub">
           <?php if(control('Permissions', FALSE)):?>
            <li><a  href="<?php echo site_url('admin/Permissions')?>"><?php echo lang('menu_permissions');?></a></li>
          <?php endif; ?>
          <?php if(control('Groups', FALSE)):?>
            <li><a  href="<?php echo site_url('admin/Groups')?>"><?php echo lang('menu_groups');?></a></li>
          <?php endif; ?>
          <?php if(control('Users', FALSE)):?>
            <li><a  href="<?php echo site_url('admin/Users')?>"><?php echo lang('menu_users');?></a></li>
          <?php endif;?>
        </ul>
      </li>
    <?php endif;?>
    <?php if(control('Masters', FALSE)):?>
        <li class="sub-menu">
          <a href="javascript:;" >
            <i class="fa fa-file-o" aria-hidden="true"></i>
            <span>Master Data</span>
          </a>
          <ul class="sub">
           <?php if(control('Stores', FALSE)):?>
            <li><a  href="<?php echo site_url('admin/Stores')?>"><?php echo lang('menu_stores');?></a></li>
          <?php endif; ?>
          <?php if(control('Events Types', FALSE)):?>
            <li><a  href="<?php echo site_url('admin/Events_types')?>"><?php echo lang('menu_events_types');?></a></li>
          <?php endif; ?>
        </ul>
      </li>
    <?php endif;?>
    <?php if(control('Customers', FALSE)):?>
        <li class="sub-menu">
          <a href="javascript:;" >
            <i class="fa fa-users" aria-hidden="true"></i>
            <span>Customers</span>
          </a>
          <ul class="sub">
           <?php if(control('Customer Details', FALSE)):?>
            <li><a  href="<?php echo site_url('admin/Customer_details')?>"><?php echo lang('menu_customer_details');?></a></li>
          <?php endif; ?>
             <?php if(control('Purchase Entries', FALSE)):?>
            <li><a  href="<?php echo site_url('admin/Purchase_entries')?>"><?php echo lang('menu_purchase_entries');?></a></li>
          <?php endif; ?>
        </ul>
      </li>
    <?php endif;?>
  </ul>
  <!-- sidebar menu end-->
</div>
</aside>