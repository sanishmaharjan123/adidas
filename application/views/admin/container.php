<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="description" content="">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <title><?php echo $header . ' | ' . config_item('site_name'); ?></title>
    <link rel="shortcut icon" href="<?php echo base_url("assets/icons/favicon.ico");?> " type="image/x-icon">

    <script type="text/javascript">
        <!--
            var base_url = '<?php echo base_url();?>';
            var index_page = "";
        // -->
    </script>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/project.min.css');?>"  />
    <!-- <link href="assets/css/bootstrap.css" rel="stylesheet"> -->
    <!--external css-->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.css');?>" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dashgum/zabuto_calendar.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/dashgum/gritter/css/jquery.gritter.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lineicons/style.css') ?>">    
    
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/css/dashgum/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/dashgum/style-responsive.css') ?>" rel="stylesheet">

    <script src="<?php echo base_url('assets/js/dashgum/chart-master/Chart.js') ?>"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

      <script type="text/javascript" src="<?php echo base_url('assets/js/project.min.js');?>"></script>

      <?php if(isset($maps) && $maps == true): ?>
        <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?libraries=places"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/googlemaps.js');?>"></script>
    <?php endif;?>

    <?php if(isset($has_upload) && $has_upload == true): ?>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.upload.js');?>"></script>
    <?php endif;?>


    <script>
            //paste this code under head tag or in a seperate js file.
            // Wait for window load
            $(window).load(function() {
                // Animate loader off screen
                // setTimeout($(".se-pre-con").fadeOut("slow"), 10000);
                $(".se-pre-con").fadeOut("slow");;
            });
        </script>
    </head>
    
    <section id="container" > <!-- Ends -->

        <!-- wrapper -->
        <!-- header logo: style can be found in header.less -->
        <?php print $this->load->view($this->config->item('template_admin') . 'header');?>

        <!-- Left side column. contains the logo and sidebar -->
        <?php print $this->load->view($this->config->item('template_admin') . 'menu');?>

        <!-- Right side column. Contains the navbar and content of the page -->
        <?php print $this->load->view($this->config->item('template_admin') . 'content');?>

        <!-- Footer -->
        <?php print $this->load->view($this->config->item('template_admin') . 'footer');?>

        <!-- ./wrapper -->
    </section>
</body>

<!-- <script src="assets/js/jquery.js"></script> -->
<!-- <script src="assets/js/jquery-1.8.3.min.js"></script> -->
<!-- <script src="assets/js/bootstrap.min.js"></script> -->
<script class="include" type="text/javascript" src="<?php echo base_url('assets/js/dashgum/jquery.dcjqaccordion.2.7.js') ?>"></script>
<script src="<?php echo base_url('assets/js/dashgum/jquery.scrollTo.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/dashgum/jquery.nicescroll.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/dashgum/jquery.sparkline.js') ?>"></script>


<!--common script for all pages-->
<script src="<?php echo base_url('assets/js/dashgum/common-scripts.js') ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/dashgum/gritter/js/jquery.gritter.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/dashgum/gritter-conf.js') ?>"></script>

<!--script for this page-->
<script src="<?php echo base_url('assets/js/dashgum/sparkline-chart.js') ?>"></script>    
<script src="<?php echo base_url('assets/js/dashgum/zabuto_calendar.js') ?>"></script>    

<script type="application/javascript">
    $(document).ready(function () {
    $("#date-popover").popover({html: true, trigger: "manual"});
    $("#date-popover").hide();
    $("#date-popover").click(function (e) {
    $(this).hide();
});

$("#my-calendar").zabuto_calendar({
action: function () {
return myDateFunction(this.id, false);
},
action_nav: function () {
return myNavFunction(this.id);
},
ajax: {
url: "show_data.php?action=1",
modal: true
},
legend: [
{type: "text", label: "Special event", badge: "00"},
{type: "block", label: "Regular event", }
]
});
});


function myNavFunction(id) {
$("#date-popover").hide();
var nav = $("#" + id).data("navigation");
var to = $("#" + id).data("to");
console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
}
</script>
</html>